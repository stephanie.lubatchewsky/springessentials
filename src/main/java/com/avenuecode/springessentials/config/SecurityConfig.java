package com.avenuecode.springessentials.config;

import com.avenuecode.springessentials.domain.EssentialsUser;
import com.avenuecode.springessentials.domain.Student;
import com.avenuecode.springessentials.repository.EssentialsUserRepository;
import com.avenuecode.springessentials.repository.StudentRepository;
import com.avenuecode.springessentials.service.EssentialsUserDetailsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;


@Slf4j
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@RequiredArgsConstructor
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final EssentialsUserDetailsService essentialsUserDetailsService;

    @Bean
    CommandLineRunner commandLineRunnerUser(EssentialsUserRepository repository) {
        return args -> {
            EssentialsUser user1 = new EssentialsUser(
                    "User XPTO",
                    "user1",
                    "{bcrypt}$2a$10$icaosDiTKNigAagqM2V3Oe52N7mLCTu74prARDFuoWcEfxxHAOpKm",
                    "ROLE_USER"
            );
            EssentialsUser admin = new EssentialsUser(
                    "Administrator",
                    "admin",
                    "{bcrypt}$2a$10$B1xwR8VMEw11SlpaXJW81OPMj/DFA4RTVwaJAQMsu1zIcFrcjlRUi",
                    "ROLE_USER,ROLE_ADMIN"
            );

            repository.saveAll(
                    List.of(user1, admin)
            );
        };
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/actuator/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .httpBasic()
                .and()
                .csrf().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        auth.userDetailsService(essentialsUserDetailsService)
                .passwordEncoder(passwordEncoder);
    }
}
