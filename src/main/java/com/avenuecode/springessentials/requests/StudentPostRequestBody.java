package com.avenuecode.springessentials.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentPostRequestBody {
    private String name;
    private String email;
    private LocalDate dob;
}
