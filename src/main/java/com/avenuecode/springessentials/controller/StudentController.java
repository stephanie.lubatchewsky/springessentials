package com.avenuecode.springessentials.controller;

import com.avenuecode.springessentials.domain.Student;
import com.avenuecode.springessentials.requests.StudentPostRequestBody;
import com.avenuecode.springessentials.requests.StudentPutRequestBody;
import com.avenuecode.springessentials.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping(path="api/v1/students", produces = MediaType.APPLICATION_JSON_VALUE)
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public ResponseEntity<List<Student>> getStudents() {
        ResponseEntity<List<Student>> returnObject = ResponseEntity.ok(studentService.getStudents());
        log.info(returnObject.toString());
        return returnObject;
    }

    @GetMapping(path="{studentId}")
    public ResponseEntity<Optional<Student>> getStudentById(
            @PathVariable("studentId") Long studentId) {
        log.info("Searching Student:: with ID " + studentId);
        Optional<Student> returnObject = studentService.getStudentById(studentId);
        if (!returnObject.isEmpty()){
            log.info(returnObject.toString());
        }
        return ResponseEntity.ok(studentService.getStudentById(studentId));
    }

    @PostMapping
    public ResponseEntity<Student> registerNewStudent(@RequestBody StudentPostRequestBody studentRequest) {
        log.info("Register Student:: " + studentRequest.toString());
        Student returnObject = studentService.addNewStudent(studentRequest);
        return new ResponseEntity<>(returnObject, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Void> updateStudent(@RequestBody StudentPutRequestBody studentRequest) {
        log.info("Update Student:: " + studentRequest.toString());
        studentService.updateStudent(studentRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(path = "{studentId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Void> deleteStudent(@PathVariable("studentId") Long studentId) {
        log.info("Deleting Student with ID :: " + studentId);
        studentService.deleteStudent(studentId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
