package com.avenuecode.springessentials.service;

import com.avenuecode.springessentials.domain.Student;
import com.avenuecode.springessentials.repository.StudentRepository;
import com.avenuecode.springessentials.requests.StudentPostRequestBody;
import com.avenuecode.springessentials.requests.StudentPutRequestBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service @Slf4j
public class StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Student> getStudents() {
        try {
            return studentRepository.findAll();
        } catch (Exception e) {
            log.info("Finding Students:: An error occurred while trying to retrieve all the students. "
                    + "\n ERROR MESSAGE: " + e.getMessage());
            log.error(e.getStackTrace().toString());
            throw e;
        }
    }

    public Optional<Student> getStudentById(Long studentId) {
        Optional<Student> returnObject = Optional.empty();
        try {
            returnObject = studentRepository.findById(studentId);
        } catch (Exception e) {
            log.info("Finding Students:: An error occurred while trying to retrieve student with ID "+ studentId
                    + ". \n ERROR MESSAGE: " + e.getMessage());
            log.error(e.getStackTrace().toString());
        }
        if(returnObject.isEmpty()){
            ResponseStatusException exception = new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "Finding Students:: Student with ID "+ studentId + " was not found");
            log.info(exception.getMessage());
            throw exception;
        }

        return studentRepository.findById(studentId);
    }

    public Student addNewStudent(StudentPostRequestBody studentRequest) {
        Student student = Student.builder()
                .name(studentRequest.getName())
                .email(studentRequest.getEmail())
                .dob(studentRequest.getDob())
                .build();

        Optional<Student> studentOptional = studentRepository
                .findStudentByEmail(student.getEmail());
        if(studentOptional.isPresent()) {
            ResponseStatusException exception = new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "Registering Student:: This email is already in use!");
            log.info(exception.getMessage());
            throw exception;
        }
        studentRepository.save(student);
        Optional<Student> exists = studentRepository.findStudentByEmail(studentRequest.getEmail());
        if (exists.isPresent()) {
            log.info("Registering Student:: Student " + exists.get().getName()
                    + " was registered with success! \n Registering Student:: ID generated: " + exists.get().getId());
        }
        else{
            log.info("Registering Student:: An error occurred while registering student " + studentRequest.getName());
        }
        return student;
    }

    public void deleteStudent(Long studentId) {
        boolean exists = studentRepository.existsById(studentId);
        if (!exists) {
            ResponseStatusException exception = new ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                    "Deleting Student:: Student with ID " + studentId + " does not exist!");
            log.info(exception.getMessage());
            throw exception;
        }
        studentRepository.deleteById(studentId);
        exists = studentRepository.existsById(studentId);
        if (!exists) {
            log.info("Deleting Student:: Student with ID " + studentId + " was deleted with success");
        }
        else{
            log.info("Deleting Student:: An error occurred while deleting student with ID " + studentId);
        }
    }

    public void updateStudent(StudentPutRequestBody studentRequest) {
        Optional<Student> studentFound = studentRepository.findById(studentRequest.getId());
        if (!studentFound.isPresent()) {
            ResponseStatusException exception = new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "Updating Student:: Student with ID " + studentRequest.getId() + " does not exist!");
            log.info(exception.getMessage());
            throw exception;
        }

        if (studentRequest.getName() != null &&
                studentRequest.getName().length() > 0 &&
        !Objects.equals(studentFound.get().getName(), studentRequest.getName())) {
            log.info("Updating Student:: Updating name "+ studentFound.get().getName()
            + " to " + studentRequest.getName());
            studentFound.get().setName(studentRequest.getName());
            studentRepository.save(studentFound.get());

            studentFound = studentRepository.findById(studentRequest.getId());
            if (!Objects.equals(studentFound.get().getName(), studentRequest.getName())) {
                ResponseStatusException exception = new ResponseStatusException(
                        HttpStatus.BAD_REQUEST,
                        "Updating Student:: An error occurred while trying to update the student's name");
                log.info(exception.getMessage());
                throw exception;
            }
            else {
                log.info("Updating Student:: Student " + studentFound.get().getName() + " updated with success!");
            }
        }
        else if (Objects.equals(studentFound.get().getName(), studentRequest.getName())) {
            ResponseStatusException exception = new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "Updating Student:: Please, inform a different name!");
            log.info(exception.getMessage());
            throw exception;
        }

        if (studentRequest.getEmail() != null &&
                studentRequest.getEmail().length() > 0 &&
                !Objects.equals(studentFound.get().getEmail(), studentRequest.getEmail())) {
            Optional<Student> studentOptional = studentRepository
                    .findStudentByEmail(studentRequest.getEmail());
            if (studentOptional.isPresent()) {
                ResponseStatusException exception = new ResponseStatusException(
                        HttpStatus.BAD_REQUEST,
                        "Updating Student:: This email is already in use!");
                log.info(exception.getMessage());
                throw exception;
            }
            log.info("Updating Student:: Updating email "+ studentFound.get().getEmail()
                    + " to " + studentRequest.getEmail());
            studentFound.get().setEmail(studentRequest.getEmail());
            studentRepository.save(studentFound.get());

            studentFound = studentRepository.findById(studentRequest.getId());
            if (!Objects.equals(studentFound.get().getEmail(), studentRequest.getEmail())) {
                ResponseStatusException exception = new ResponseStatusException(
                        HttpStatus.BAD_REQUEST,
                        "Updating Student:: An error occurred while trying to update the student's email");
                log.info(exception.getMessage());
                throw exception;
            }
            else {
                log.info("Updating Student:: Student " + studentFound.get().getName() + " updated with success!");
            }
        }
    }
}
