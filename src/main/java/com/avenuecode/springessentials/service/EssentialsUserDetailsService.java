package com.avenuecode.springessentials.service;

import com.avenuecode.springessentials.repository.EssentialsUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EssentialsUserDetailsService implements UserDetailsService {
    private final EssentialsUserRepository essentialsUserRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        return Optional.ofNullable(essentialsUserRepository.findByUsername(username))
                .orElseThrow(()-> new UsernameNotFoundException("Spring Essentials User not found!"));
    }
}
