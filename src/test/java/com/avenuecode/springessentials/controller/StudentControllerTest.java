package com.avenuecode.springessentials.controller;

import com.avenuecode.springessentials.domain.Student;
import com.avenuecode.springessentials.requests.StudentPostRequestBody;
import com.avenuecode.springessentials.requests.StudentPutRequestBody;
import com.avenuecode.springessentials.service.StudentService;
import com.avenuecode.springessentials.util.StudentCreator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@DisplayName("Tests for Student Controller")
class StudentControllerTest {

    @InjectMocks
    private StudentController studentController;

    @Mock
    private StudentService studentServiceMock;

    @BeforeEach
    void setUp() {
        List<Student> studentList = new ArrayList<Student>();
        studentList.add(StudentCreator.createStudent());
        BDDMockito.when(studentServiceMock.getStudents())
                  .thenReturn(studentList);

        BDDMockito.when(studentServiceMock.getStudentById(ArgumentMatchers.anyLong()))
                .thenReturn(Optional.ofNullable(StudentCreator.createStudent()));

        BDDMockito.when(studentServiceMock.addNewStudent(ArgumentMatchers.any(StudentPostRequestBody.class)))
                .thenReturn(StudentCreator.createStudent());

        BDDMockito.doNothing().when(studentServiceMock).updateStudent(ArgumentMatchers.any(StudentPutRequestBody.class));

        BDDMockito.doNothing().when(studentServiceMock).deleteStudent(ArgumentMatchers.anyLong());

    }

    @Test
    @DisplayName("Must return a list with students when successful")
    void getStudentsTest() {
        String expectedName = StudentCreator.createStudent().getName();

        List<Student> students = studentController.getStudents().getBody();

        Assertions.assertThat(students).isNotNull()
                .isNotEmpty()
                .hasSizeGreaterThan(0);
        Assertions.assertThat(students.get(0).getName()).isEqualTo(expectedName);
    }

    @Test
    @DisplayName("Must return an specific student when successful")
    void getStudentByIdTest() {
        String expectedName = StudentCreator.createStudent().getName();

        Optional<Student> studentFound = studentController.getStudentById(1l).getBody();

        Assertions.assertThat(studentFound).isNotNull();
        Assertions.assertThat(studentFound.get().getName()).isEqualTo(expectedName);
    }

    @Test
    @DisplayName("Must register a new student when successful")
    void registerNewStudentTest() {
        Student studentCreated = studentController.registerNewStudent(StudentCreator.createStudentPostRB()).getBody();

        Assertions.assertThat(studentCreated).isNotNull();
        Assertions.assertThat(studentCreated.getName()).isEqualTo(StudentCreator.createStudent().getName());
        Assertions.assertThat(studentController.registerNewStudent(StudentCreator.createStudentPostRB())
                .getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    @DisplayName("Must update a student when successful")
    void updateStudentTest() {
        studentController.updateStudent(StudentCreator.createStudentPutRB());

        Assertions.assertThatCode(() -> studentController.updateStudent(StudentCreator.createStudentPutRB()))
                .doesNotThrowAnyException();

        ResponseEntity<Void> entity = studentController.updateStudent(StudentCreator.createStudentPutRB());
        Assertions.assertThat(entity).isNotNull();
        Assertions.assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @DisplayName("Must remove a student when successful")
    void deleteStudentTest() {
        Assertions.assertThatCode(() -> studentController.deleteStudent(1l))
                .doesNotThrowAnyException();

        ResponseEntity<Void> entity = studentController.deleteStudent(1l);
        Assertions.assertThat(entity).isNotNull();
        Assertions.assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}