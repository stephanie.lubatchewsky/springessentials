package com.avenuecode.springessentials.integration;

import com.avenuecode.springessentials.domain.Student;
import com.avenuecode.springessentials.repository.StudentRepository;
import com.avenuecode.springessentials.requests.StudentPostRequestBody;
import com.avenuecode.springessentials.requests.StudentPutRequestBody;
import com.avenuecode.springessentials.util.StudentCreator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@DisplayName("Integration Tests for Student Controller")
class StudentControllerIT {
    @Autowired
    @Qualifier(value = "testRestTemplateRoleUser")
    private TestRestTemplate testRestTemplateUser;
    @Autowired
    @Qualifier(value = "testRestTemplateRoleAdmin")
    private TestRestTemplate testRestTemplateAdmin;
    @Autowired
    private StudentRepository studentRepository;

    @TestConfiguration
    @Lazy
    static class Config {
        @Bean(name = "testRestTemplateRoleUser")
        public TestRestTemplate testRestTemplateRoleUserCreator(@Value("${local.server.port}") int port) {
            RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder()
                    .rootUri("http://localhost:" + port)
                    .basicAuthentication("user1", "pass123");
            return new TestRestTemplate(restTemplateBuilder);
        }

        @Bean(name = "testRestTemplateRoleAdmin")
        public TestRestTemplate testRestTemplateRoleAdminCreator(@Value("${local.server.port}") int port) {
            RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder()
                    .rootUri("http://localhost:" + port)
                    .basicAuthentication("admin", "admin");
            return new TestRestTemplate(restTemplateBuilder);
        }
    }

    @Test
    @DisplayName("Must return a list with students when successful")
    void getStudentsTest() {
        Student expectedStudent = studentRepository.save(StudentCreator.createStudentWithoutId());

        List<Student> students = testRestTemplateUser.exchange("/api/v1/students/",
                HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Student>>() {
                }).getBody();

        Assertions.assertThat(students).isNotNull()
                .isNotEmpty()
                .hasSizeGreaterThan(0);
        Assertions.assertThat(students.get(2)).isEqualTo(expectedStudent);
    }

    @Test
    @DisplayName("Must return an specific student when successful")
    void getStudentByIdTest() {
        Student expectedStudent = studentRepository.save(StudentCreator.createStudentWithoutId());

        Student student = testRestTemplateUser.getForObject("/api/v1/students/{studentId}",
                Student.class, expectedStudent.getId());
        Assertions.assertThat(student).isNotNull();
        Assertions.assertThat(student.getId()).isEqualTo(expectedStudent.getId());
    }

    @Test
    @DisplayName("Must register a new student when successful")
    void registerNewStudentTest() {
        StudentPostRequestBody studentPostRequestBody = StudentCreator.createStudentPostRB();

        ResponseEntity<Student> studentResponseEntity = testRestTemplateUser.postForEntity("/api/v1/students/",
                studentPostRequestBody, Student.class);

        Assertions.assertThat(studentResponseEntity).isNotNull();
        Assertions.assertThat(studentResponseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        Assertions.assertThat(studentResponseEntity.getBody()).isNotNull();
        Assertions.assertThat(studentResponseEntity.getBody().getId()).isNotNull();

    }

    @Test
    @DisplayName("Must update a student when successful")
    void updateStudentTest() {

        Student savedStudent = studentRepository.save(StudentCreator.createStudentWithoutId());
        savedStudent.setName("Jack");

        ResponseEntity<Void> studentResponseEntity = testRestTemplateUser.exchange("/api/v1/students/",
                HttpMethod.PUT, new HttpEntity<>(savedStudent), Void.class);

        Assertions.assertThat(studentResponseEntity).isNotNull();
        Assertions.assertThat(studentResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @DisplayName("Must remove a student when successful")
    void deleteStudentTest() {
        Student savedStudent = studentRepository.save(StudentCreator.createStudentWithoutId());

        ResponseEntity<Void> animeResponseEntity = testRestTemplateAdmin.exchange("/api/v1/students/{studentId}",
                HttpMethod.DELETE, null, Void.class, savedStudent.getId());

        Assertions.assertThat(animeResponseEntity).isNotNull();
        Assertions.assertThat(animeResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @DisplayName("Must return a 403 when user's role is not admin")
    void deleteStudentForbiddenTest() {
        Student savedStudent = studentRepository.save(StudentCreator.createStudentWithoutId());

        ResponseEntity<Void> animeResponseEntity = testRestTemplateUser.exchange("/api/v1/students/{studentId}",
                HttpMethod.DELETE, null, Void.class, savedStudent.getId());

        Assertions.assertThat(animeResponseEntity).isNotNull();
        Assertions.assertThat(animeResponseEntity.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }
}
