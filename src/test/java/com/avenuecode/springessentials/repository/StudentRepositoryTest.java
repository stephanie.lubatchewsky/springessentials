package com.avenuecode.springessentials.repository;

import com.avenuecode.springessentials.domain.Student;
import com.avenuecode.springessentials.util.StudentCreator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@DisplayName("Tests for Student Repository")
class StudentRepositoryTest {

    @Autowired
    private StudentRepository studentRepository;

    @Test
    @DisplayName("Must create a new student when successful")
    public void saveStudentTest() {
        Student studentToBeSaved = StudentCreator.createStudent();
        Student studentSaved = this.studentRepository.save(studentToBeSaved);
        Assertions.assertThat(studentSaved).isNotNull();
        Assertions.assertThat(studentSaved.getId()).isNotNull();
        Assertions.assertThat(studentSaved.getName()).isEqualTo(studentToBeSaved.getName());
        Assertions.assertThat(studentSaved.getEmail()).isEqualTo(studentToBeSaved.getEmail());
        Assertions.assertThat(studentSaved.getDob()).isEqualTo(studentToBeSaved.getDob());
        Assertions.assertThat(studentSaved.getAge()).isNotNull()
                                                    .isGreaterThan(0)
                                                    .isNotNegative();
    }

    @Test
    @DisplayName("Must update the student when successful")
    public void updateStudentTest() {
        Student studentSaved = this.studentRepository.save(StudentCreator.createStudent());

        studentSaved.setName("Updating Guy");
        Student studentUpdated = this.studentRepository.save(studentSaved);
        Assertions.assertThat(studentSaved).isNotNull();
        Assertions.assertThat(studentUpdated.getId()).isEqualTo(studentSaved.getId());
        Assertions.assertThat(studentUpdated.getName()).isEqualTo(studentSaved.getName());
        Assertions.assertThat(studentUpdated.getEmail()).isEqualTo(studentSaved.getEmail());
        Assertions.assertThat(studentUpdated.getDob()).isEqualTo(studentSaved.getDob());
        Assertions.assertThat(studentUpdated.getAge()).isNotNull();
    }

    @Test
    @DisplayName("Must remove the student when successful")
    public void deleteStudentTest() {
        Student studentSaved = this.studentRepository.save(StudentCreator.createStudent());

        this.studentRepository.deleteById(studentSaved.getId());
        Optional<Student> studentOptional = this.studentRepository.findById(studentSaved.getId());
        Assertions.assertThat(studentOptional).isEmpty();
    }

    @Test
    @DisplayName("Must return an specific student when successful")
    public void findStudentByEmailTest() {
        Student studentSaved = this.studentRepository.save(StudentCreator.createStudent());

        String email = studentSaved.getEmail();
        Optional<Student> studentFound = this.studentRepository.findStudentByEmail(email);
        Assertions.assertThat(studentFound).isNotNull();
        Assertions.assertThat(studentFound.get().getEmail()).isEqualTo(email);
    }

    @Test
    @DisplayName("Must return an empty object when student is not found")
    public void notFindStudentByEmailTest() {
        Student studentSaved = this.studentRepository.save(StudentCreator.createStudent());

        String email = "notfound@mail.com";
        Optional<Student> studentOptional = this.studentRepository.findStudentByEmail(email);
        Assertions.assertThat(studentOptional).isEmpty();
    }
}