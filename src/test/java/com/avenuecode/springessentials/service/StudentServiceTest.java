package com.avenuecode.springessentials.service;

import com.avenuecode.springessentials.domain.Student;
import com.avenuecode.springessentials.repository.StudentRepository;
import com.avenuecode.springessentials.util.StudentCreator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@DisplayName("Tests for Student Service")
class StudentServiceTest {

    @InjectMocks
    private StudentService studentService;

    @Mock
    private StudentRepository studentRepositoryMock;

    @BeforeEach
    void setUp() {
        List<Student> studentList = new ArrayList<Student>();
        studentList.add(StudentCreator.createStudent());
        BDDMockito.when(studentRepositoryMock.findAll())
                .thenReturn(studentList);

        BDDMockito.when(studentRepositoryMock.findById(ArgumentMatchers.anyLong()))
                .thenReturn(Optional.ofNullable(StudentCreator.createStudent()));

        BDDMockito.when(studentRepositoryMock.save(ArgumentMatchers.any(Student.class)))
                .thenReturn(StudentCreator.createStudent());

        BDDMockito.when(studentRepositoryMock.existsById(ArgumentMatchers.anyLong()))
                        .thenReturn(true);
        BDDMockito.doNothing().when(studentRepositoryMock).deleteById(ArgumentMatchers.anyLong());

    }

    @Test
    @DisplayName("Must return a list with students when successful")
    void getStudentsTest() {
        String expectedName = StudentCreator.createStudent().getName();

        List<Student> students = studentService.getStudents();

        Assertions.assertThat(students).isNotNull()
                .isNotEmpty()
                .hasSizeGreaterThan(0);
        Assertions.assertThat(students.get(0).getName()).isEqualTo(expectedName);
    }

    @Test
    @DisplayName("Must return an specific student when successful")
    void getStudentByIdTest() {
        String expectedName = StudentCreator.createStudent().getName();

        Optional<Student> studentFound = studentService.getStudentById(1l);

        Assertions.assertThat(studentFound).isNotNull();
        Assertions.assertThat(studentFound.get().getName()).isEqualTo(expectedName);
    }

    @Test
    @DisplayName("Must register a new student when successful")
    void registerNewStudentTest() {
        Student studentCreated = studentService.addNewStudent(StudentCreator.createStudentPostRB());

        Assertions.assertThat(studentCreated).isNotNull();
        Assertions.assertThat(studentCreated.getName()).isEqualTo(StudentCreator.createStudent().getName());
    }

    @Test
    @DisplayName("Must throw exception when trying to register a new student")
    void registerNewStudentExceptionTest() {
        BDDMockito.when(studentRepositoryMock.findStudentByEmail(ArgumentMatchers.anyString()))
                .thenReturn(Optional.ofNullable(StudentCreator.createStudent()));

        Assertions.assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> this.studentService.addNewStudent(StudentCreator.createStudentPostRB()))
                .withMessageContaining("400 BAD_REQUEST");
    }

    @Test
    @DisplayName("Must update a student when successful")
    void updateStudentTest() {
        Assertions.assertThatCode(() -> studentService.updateStudent(StudentCreator.createStudentWithIdPutRB()))
                .doesNotThrowAnyException();
    }

    @Test
    @DisplayName("Must throw an exception for invalid student name")
    void updateStudentNameExceptionTest() {
        Assertions.assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> this.studentService.updateStudent(StudentCreator.createStudentPutRB()))
                .withMessageContaining("400 BAD_REQUEST");
    }

    @Test
    @DisplayName("Must throw an exception for different student email")
    void updateStudentDifferentEmailExceptionTest() {
        Student studentForUpdate = StudentCreator.createInvalidEmailStudent();

        Assertions.assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> this.studentService.updateStudent(StudentCreator.createStudentPutRB()))
                .withMessageContaining("400 BAD_REQUEST");
    }

    @Test
    @DisplayName("Must throw an exception for different student email")
    void updateStudentAlreadyInUseEmailExceptionTest() {
        BDDMockito.when(studentRepositoryMock.findStudentByEmail(ArgumentMatchers.anyString()))
                .thenReturn(Optional.ofNullable(StudentCreator.createSecondStudent()));

        Student secondStudent = StudentCreator.createSecondStudent();
        Student studentForUpdate = StudentCreator.createInvalidEmailStudent();

        Assertions.assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> this.studentService.updateStudent(StudentCreator.createStudentPutRB()))
                .withMessageContaining("400 BAD_REQUEST");
    }
    @Test
    @DisplayName("Must remove a student when successful")
    void deleteStudentTest() {
        Assertions.assertThatCode(() -> studentService.deleteStudent(1l))
                .doesNotThrowAnyException();
    }

    @Test
    @DisplayName("Must throw exception when trying to remove a student")
    void deleteStudentExceptionTest() {
        BDDMockito.when(studentRepositoryMock.existsById(ArgumentMatchers.anyLong()))
                .thenReturn(false);

        Assertions.assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> this.studentService.deleteStudent(1l))
                .withMessageContaining("400 BAD_REQUEST");
    }
}