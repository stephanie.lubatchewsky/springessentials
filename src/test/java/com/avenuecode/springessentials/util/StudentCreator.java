package com.avenuecode.springessentials.util;

import com.avenuecode.springessentials.domain.Student;
import com.avenuecode.springessentials.requests.StudentPostRequestBody;
import com.avenuecode.springessentials.requests.StudentPutRequestBody;

import java.time.LocalDate;

public class StudentCreator {

    public static Student createStudent(){
        return Student.builder()
                .id(1l)
                .name("Testing Guy")
                .email("testing@mail.com")
                .dob(LocalDate.parse("1991-12-12"))
                .build();
    }

    public static Student createSecondStudent(){
        return Student.builder()
                .id(2l)
                .name("Testing Guy2")
                .email("testing2@mail.com")
                .dob(LocalDate.parse("1991-12-12"))
                .build();
    }

    public static Student createInvalidEmailStudent(){
        return Student.builder()
                .id(1l)
                .name("Updated Guy")
                .email("testing@mail.com")
                .dob(LocalDate.parse("1991-12-12"))
                .build();
    }

    public static Student createStudentWithoutId(){
        return Student.builder()
                .name("Jackson")
                .email("jackson@mail.com")
                .dob(LocalDate.parse("1991-12-12"))
                .build();
    }

    public static StudentPostRequestBody createStudentPostRB(){
        return StudentPostRequestBody.builder()
                .name("Testing Guy")
                .email("testing@mail.com")
                .dob(LocalDate.parse("1991-12-12"))
                .build();
    }

    public static StudentPutRequestBody createStudentPutRB(){
        return StudentPutRequestBody.builder()
                .name("Updating Guy")
                .email("updating@mail.com")
                .dob(LocalDate.parse("1991-12-12"))
                .build();
    }

    public static StudentPutRequestBody createStudentWithIdPutRB(){
        return StudentPutRequestBody.builder()
                .id(4l)
                .name("Updating Guy")
                .email("updating@mail.com")
                .dob(LocalDate.parse("1991-12-12"))
                .build();
    }

    public static StudentPutRequestBody createInvalidIdStudentPutRB(){
        return StudentPutRequestBody.builder()
                .id(3l)
                .name("Updated Guy")
                .email("updating@mail.com")
                .dob(LocalDate.parse("1991-12-12"))
                .build();
    }

    public static StudentPutRequestBody createStudentPutPBFromStudent(Student student){
        return StudentPutRequestBody.builder()
                .id(student.getId())
                .name(student.getName())
                .email(student.getEmail())
                .dob(student.getDob())
                .build();
    }
}
