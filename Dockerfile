FROM openjdk

RUN mkdir /usr/src/app

WORKDIR /usr/src/app

COPY ./target /usr/src/app

EXPOSE 8080

ENTRYPOINT java -jar springessentials-0.0.1-SNAPSHOT.jar

